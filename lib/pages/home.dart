import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  TextEditingController weightController = TextEditingController();
  TextEditingController heightController = TextEditingController();

  String _infoText = "Informe seus dados!";

  void _resetFields() {
    weightController.text = "";
    heightController.text = "";
    setState(() {
      _infoText = "Informe seus dados!";
      _formKey = GlobalKey<FormState>();
    });
  }

  void _calculate() {
    double height = double.parse(heightController.text);
    double weight = double.parse(weightController.text);
    double imc = weight / (height * height);

    String newValue = "";

    if (imc < 18.6) {
      newValue = "Abaixo do peso! (${imc.toStringAsPrecision(4)})";
    } else if (imc >= 18.6 && imc < 24.9) {
      newValue = "Peso ideal! (${imc.toStringAsPrecision(4)})";
    } else if (imc >= 24.9 && imc < 29.9) {
      newValue = "Levemente acima do peso! (${imc.toStringAsPrecision(4)})";
    } else if (imc >= 29.9 && imc < 34.4) {
      newValue = "Obesidade grau I! (${imc.toStringAsPrecision(4)})";
    } else if (imc >= 34.9 && imc < 39.9) {
      newValue = "Obesidade grau II (${imc.toStringAsPrecision(4)})";
    }

    setState(() {
      _infoText = newValue;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Calcular IMC"),
          centerTitle: true,
          backgroundColor: Colors.green,
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.refresh,
              ),
              onPressed: _resetFields,
            )
          ],
        ),
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          padding: const EdgeInsets.only(left: 40, right: 40),
          child: Form(
              key: _formKey,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Icon(Icons.person_outline, size: 120, color: Colors.green),
                    TextFormField(
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          labelText: "Peso em KG",
                        ),
                        controller: weightController,
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Insira o peso!";
                          }
                        }),
                    TextFormField(
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          labelText: "Altura em CM",
                        ),
                        controller: heightController,
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Insira a altura!";
                          }
                        }),
                    Padding(
                      padding: EdgeInsets.only(top: 40, bottom: 40),
                      child: Text(
                        _infoText,
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                        margin: const EdgeInsets.only(left: 50, right: 50),
                        height: 60,
                        child: RaisedButton(
                          onPressed: () {
                            if (_formKey.currentState.validate()) {
                              return _calculate();
                            }
                          },
                          color: Colors.green,
                          child: Text("Calcular",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 18)),
                        ))
                  ])),
        ));
  }
}
